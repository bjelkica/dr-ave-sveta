# -*- encoding: utf-8 -*-

from tkinter import *
from tkinter import ttk

import requests
import re


stran1=requests.get("https://www.cia.gov/library/publications/the-world-factbook/")
vzorec1=re.compile(r'<option value="geos/(.*).html"'+ '.*' + '</option>' )
seznam_kratic=re.findall(r'<option value="geos/(.*).html"'+ '.*' + '</option>',stran1.text)
seznam_drzav=re.findall(r'<option value="geos/.*.html">'+ '(.*)' + '</option>',stran1.text)


seznamDrzav=[]
for i in seznam_drzav:
    seznamDrzav.append(i.strip())

stran5=requests.get("https://www.cia.gov/library/publications/the-world-factbook/rankorder/2119rank.html")
sez=list(seznamDrzav)

for i in sez:
    po2=re.findall('.html>{0}</a></td><td>(.*?)</td><td>July 2014 est.</td></tr>'.format(i),str(stran5.content))
    if po2==[]:
        sez.remove(i)
sez.remove("Atlantic Ocean")
  
    
nov_seznam=[]
for j in seznam_drzav:
    nov_seznam.append(j.strip())

slovar={}
for i in range(len(seznam_drzav)):
    slovar[nov_seznam[i]]=seznam_kratic[i]


stran2=requests.get("http://www.scri8e.com/white.gif")
with open ("platno.gif","wb") as f:
    f.write(stran2.content)

class Countries():
    def __init__(self, master):
        self.drzava=StringVar()
        self.history=StringVar()
        self.location=StringVar()
        self.area=StringVar()
        self.climate=StringVar()
        self.terrain=StringVar()
        self.natural_resources=StringVar()
        self.natural_disasters=StringVar()
        self.environment_issues=StringVar()
        self.nationality=StringVar()
        self.ethnic=StringVar()
        self.language=StringVar()
        self.religion=StringVar()
        self.population=StringVar()
        self.birth_rate=StringVar()
        self.death_rate=StringVar()
        self.name=StringVar()
        self.capital=StringVar()
        self.government_type=StringVar()
        self.division=StringVar()
        self.holiday=StringVar()
        self.economy=StringVar()
      

        self.master=master
        self.menu=Menu(master)
        
        self.podmenu=Menu(self.menu)
        self.podmenu.add_command(label="Quit",command=self.konec)

        self.menu.add_cascade(label="File",menu=self.podmenu)
        self.menu.add_cascade(label="Country info",command=self.drzave)
        self.menu.add_cascade(label="Compare",command=self.Risi)
        
        master.config(menu=self.menu)
        self.drzave()

        
    def drzave(self):
            for i in self.master.children.values():
                i.grid_remove()
                
            self.okvir=Frame(self.master)
            self.okvir.grid(row=0,column=0,)
            Label(self.okvir,text="Country name : ").grid(row=0,column=0)
            Label(self.okvir,textvariable=self.drzava).grid(row=0,column=1)
            
            

            polje=ttk.Combobox(self.okvir,textvariable=self.drzava)
            polje["values"]=tuple(seznamDrzav)
            polje.grid(row=0,column=1)
            polje.focus()
            polje.bind("<Return>", self.isci)
            
            gumb=Button(self.okvir,text="Search",command=self.isci,relief="solid",bd=1)
            gumb.grid(row=0,column=2)

            
            self.platno2=Canvas(self.master,scrollregion=(0,0,2000,2000),background="white",width=700,height=750)
            self.okvir2=Frame(self.platno2,background="white",relief="solid",bd=1)

            s=Scrollbar(self.master,orient=HORIZONTAL)
            s.config(command=self.platno2.xview)    
            
            self.platno2.config(xscrollcommand=s.set)
            s.grid(row=29,column=6,ipadx=50)
            self.platno2.grid(row=0,column=6)

            
            Label(self.okvir2,text="Name : ").grid(row=0,column=5,sticky=W)
            
            Label(self.okvir2,textvariable=self.name,anchor=W).grid(row=0,column=6,sticky=W)
            Label(self.okvir2,text="Location : ").grid(row=1,column=5,sticky=W)
            Label(self.okvir2,textvariable=self.location,justify=LEFT).grid(row=1,column=6,sticky=W)
            Label(self.okvir2,text="Capital city : ").grid(row=2,column=5,sticky=W)
            Label(self.okvir2,textvariable=self.capital,justify=LEFT).grid(row=2,column=6,sticky=W)
            Label(self.okvir2,text="Area : ").grid(row=3,column=5,sticky=W)
            Label(self.okvir2,textvariable=self.area,justify=LEFT).grid(row=3,column=6,sticky=W)
            Label(self.okvir2,text="Division :").grid(row=4,column=5,sticky=W)
            Label(self.okvir2,textvariable=self.division,justify=LEFT).grid(row=4,column=6,sticky=W)

            Label(self.okvir2).grid(row=5,column=5,sticky=W)
            Label(self.okvir2,text="Climate : ").grid(row=6,column=5,sticky=W)
            Label(self.okvir2,textvariable=self.climate,justify=LEFT).grid(row=6,column=6,sticky=W)
            Label(self.okvir2,text="Terrain : ").grid(row=7,column=5,sticky=W)
            Label(self.okvir2,textvariable=self.terrain,justify=LEFT).grid(row=7,column=6,sticky=W) 
            Label(self.okvir2,text="Natural resources : ").grid(row=8,column=5,sticky=W)
            Label(self.okvir2,textvariable=self.natural_resources,justify=LEFT).grid(row=8,column=6,sticky=W)
            Label(self.okvir2,text="Natural disasters : ").grid(row=9,column=5,sticky=W)
            Label(self.okvir2,textvariable=self.natural_disasters,justify=LEFT).grid(row=9,column=6,sticky=W)
            Label(self.okvir2,text="Environment issues : ").grid(row=10,column=5,sticky=W)
            Label(self.okvir2,textvariable=self.environment_issues,justify=LEFT).grid(row=10,column=6,sticky=W)

            Label(self.okvir2).grid(row=11,column=5)
            Label(self.okvir2,text="Nationality :").grid(row=12,column=5,sticky=W)
            Label(self.okvir2,textvariable=self.nationality,justify=LEFT).grid(row=12,column=6,sticky=W)
            Label(self.okvir2,text="Ethnic groups :").grid(row=13,column=5,sticky=W)
            Label(self.okvir2,textvariable=self.ethnic,justify=LEFT).grid(row=13,column=6,sticky=W)
            Label(self.okvir2,text="Religions :").grid(row=14,column=5,sticky=W)
            Label(self.okvir2,textvariable=self.religion,justify=LEFT).grid(row=14,column=6,sticky=W)
            Label(self.okvir2,text="Languages :").grid(row=15,column=5,sticky=W)
            Label(self.okvir2,textvariable=self.language,justify=LEFT).grid(row=15,column=6,sticky=W)

            Label(self.okvir2).grid(row=16,column=5)
            Label(self.okvir2,text="Population :").grid(row=17,column=5,sticky=W)
            Label(self.okvir2,textvariable=self.population,justify=LEFT).grid(row=17,column=6,sticky=W)
            Label(self.okvir2,text="Birth rate :").grid(row=18,column=5,sticky=W)
            Label(self.okvir2,textvariable=self.birth_rate,justify=LEFT).grid(row=18,column=6,sticky=W)
            Label(self.okvir2,text="Death rate :").grid(row=19,column=5,sticky=W)
            Label(self.okvir2,textvariable=self.death_rate,justify=LEFT).grid(row=19,column=6,sticky=W)

            Label(self.okvir2).grid(row=20,column=5)
            Label(self.okvir2,text="Government type :").grid(row=21,column=5,sticky=W)
            Label(self.okvir2,textvariable=self.government_type,justify=LEFT).grid(row=21,column=6,sticky=W)
            Label(self.okvir2,text="National holiday :").grid(row=22,column=5,sticky=W)
            Label(self.okvir2,textvariable=self.holiday,justify=LEFT).grid(row=22,column=6,sticky=W)
            
            Label(self.okvir2).grid(row=23,column=5)

            Label(self.okvir2,text="Economy overwiev :").grid(row=24,column=5,sticky=W)
            Label(self.okvir2,textvariable=self.economy,justify=LEFT,wraplength=1000).grid(row=25,column=6,sticky=W)
            Label(self.okvir2,text=" History :").grid(row=26,column=5,sticky=W)
            Label(self.okvir2,textvariable=self.history,justify=LEFT,wraplength=1000).grid(row=27,column=6,sticky=W)                                              

            self.platno2.create_window((0,0),window=self.okvir2,anchor=NW)
            
            self.canvas = Canvas(self.okvir,width=400,height=400,relief="solid",bd=1)
            self.canvas.grid(row=1, column=1)
            self.sli=PhotoImage(file="platno.gif",width=300,height=300)
            self.a=self.canvas.create_image(20, 20,anchor=NW ,image=self.sli)     
        
    def konec(self,*args):
        root.destroy()


    
    def Config(self,event):
        self.platno2.configure(scrollregion=self.platno2.bbox("all"),width=800,height=800)
        

        
        
    def isci(self,*args):
   
        ime=self.drzava.get()
        
        try:


            kratica=slovar[ime]

            stran=requests.get("https://www.cia.gov/library/publications/the-world-factbook/geos/{0}.html".format(str(kratica)))
            

            a=re.findall(r'Background.*?<div class=category_data>(.*?)</div>',str(stran.content))
            if a==[]:
                self.history.set("")
            else:
                self.history.set(a[0])
            
            b=re.findall(r'Location.*?<div class=category_data>(.*?)</div>',str(stran.content))
            if b==[]:
                self.location.set("")
            else:
                self.location.set(b[0])

            c=re.findall(r'<span class=category>total: </span><span class=category_data>(.*?)</span></div>',str(stran.content))
            if c==[]:
                self.area.set("")
            else:
                 self.area.set(c[0])
            
            d=re.findall(r'name:.*?<span class="category_data" style="font-weight:normal; vertical-align:bottom;">(.*?)</span></div>',str(stran.content))
            if d==[]:
                self.capital.set("")
            else:
                self.capital.set(d[0])

            e=re.findall(r'Climate.*?<div class=category_data>(.*?)</div>',str(stran.content))
            if e==[]:
                self.climate.set("")
            else:
                self.climate.set(e[0])

            f=re.findall(r'Terrain.*?<div class=category_data>(.*?)</div>',str(stran.content))
            if f==[]:
                self.terrain.set("")
            else:
                self.terrain.set(f[0])
            
            g=re.findall(r'Natural resources.*?<div class=category_data>(.*?)</div>',str(stran.content))
            if g==[]:
                self.natural_resources.set("")
            else:
                self.natural_resources.set(g[0])
                
            h=re.findall(r'Natural hazards.*?<div class=category_data>(.*?)</div>',str(stran.content))
            if h==[]:
                self.natural_disasters.set("")
            else:
                self.natural_disasters.set(h[0])
                
            i=re.findall(r'Environment - current issues.*?<div class=category_data>(.*?)</div>',str(stran.content))
            if i==[]:
                self.environment_issues.set("")
            else:
                self.environment_issues.set(i[0])

            
            j=re.findall(r'Nationality.*?<span class=category>noun: </span><span class=category_data>(.*?)</span></div>',str(stran.content))
            if j==[]:
                self.nationality.set("")
            else:
                self.nationality.set(j[0])

            k=re.findall(r'Ethnic groups.*?<div class=category_data>(.*?)</div>',str(stran.content))
            if k==[]:
                self.ethnic.set("")
            else:
                self.ethnic.set(k[0])

            l=re.findall(r'Languages:.*?<div class=category_data>(.*?)</div>',str(stran.content))
            if l==[]:
                self.language.set("")
            else:
                self.language.set(l[0])

            m=re.findall(r'Religions.*?<div class=category_data>(.*?)</div>',str(stran.content))
            if m==[]:
                self.religion.set("")
            else:
                self.religion.set(m[0])

            n=re.findall(r'Population.*?<div class=category_data>(.*?)</div>',str(stran.content))
            if n==[]:
                self.population.set("")
            else:
                self.population.set(n[0])

            o=re.findall(r'Birth rate.*?<div class=category_data>(.*?)</div>',str(stran.content))
            if o==[]:
                self.birth_rate.set("")
            else:
                self.birth_rate.set(o[0])

            p=re.findall(r'Death rate.*?<div class=category_data>(.*?)</div>',str(stran.content))
            if p==[]:
                self.death_rate.set("")
            else:
                self.death_rate.set(p[0])

            q=re.findall(r'<span class=category>conventional short form: </span><span class=category_data>(.*?)</span></div>',str(stran.content))
            if q==[]:
                self.name.set("")
            else:
                self.name.set(q[0])

            r=re.findall(r'Government type.*?<div class=category_data>(.*?)</div>',str(stran.content))
            if r==[]:
                self.government_type.set("")
            else:
                self.government_type.set(r[0])

            s=re.findall(r'Administrative divisions.*?<div class=category_data>(.*?)</div>',str(stran.content))
            if s==[]:
                self.division.set("")
            else:
                self.division.set(s[0])
                              
            t=re.findall(r'National holiday.*?<div class=category_data>(.*?)</div>',str(stran.content))
            if t==[]:
                self.holiday.set("")
            else:
                self.holiday.set(t[0])
            
            
            

            u=re.findall(r'Economy - overview.*?<div class=category_data>(.*?)</div>',str(stran.content))
            if u==[]:
                self.economy.set("")
            else:
                self.economy.set(u[0])

           
                 
            sl=requests.get("https://www.cia.gov/library/publications/the-world-factbook/graphics/maps/{0}-map.gif".format(kratica))
            with open ("slika.gif","wb") as f:
                f.write(sl.content)
            
            
            self.img=PhotoImage(file="slika.gif")
            self.canvas.itemconfig(self.a, image = self.img)
      
       

        except KeyError:
            print("You entered the wrong name.")
            self.name.set("You entered the wrong name.")
       

    def Risi(self):
        self.var=StringVar()
        self.var.set("area")
        self.var2=StringVar()
        self.var2.set(seznamDrzav[1])
        self.var3=StringVar()
        self.var3.set(seznamDrzav[4])
        self.var4=StringVar()
        self.var4.set(seznamDrzav[3])
        
        for i in self.master.children.values():
            i.grid_remove()

     
        opcije=sez
        self.oz=Label(self.master,text="Compare by: ",bg="yellow").grid(row=0,column=0)
        self.option=OptionMenu(self.master,self.var,"area","population")
        self.option.grid(row=0,column=1)

        self.oz2=Label(self.master,text="Country1: ",bg="yellow").grid(row=0,column=3,sticky=E)
        self.option2=OptionMenu(self.master,self.var2,*opcije)
        self.option2["menu"].config(bg="yellow")
        self.option2.grid(row=0,column=4,sticky=W)

        self.oz3=Label(self.master,text="Country2: ",bg="yellow").grid(row=1,column=3,sticky=W)
        self.option3=OptionMenu(self.master,self.var3,*opcije)
        self.option3["menu"].config(bg="yellow")
        self.option3.grid(row=1,column=4,sticky=W)

        self.oz4=Label(self.master,text="Country3: ",bg="yellow").grid(row=2,column=3,sticky=W)
        self.option4=OptionMenu(self.master,self.var4,*opcije)
        self.option4["menu"].config(bg="yellow")
        self.option4.grid(row=2,column=4,sticky=W)
        

        self.g_width=600
        self.g_height=600
        self.platno=Canvas(self.master,width=self.g_width,height=self.g_height,relief="solid",bd=1)
        self.platno.grid(row=3,column=2)

        self.gumbek=Button(self.master,text="Go!",command=self.graf,relief="solid",bd=1,bg="yellow")
        self.gumbek.grid(row=2,column=5)

    def graf(self):
        
        self.platno.delete("all")
        var=self.var.get()
        var2=self.var2.get()
        var3=self.var3.get()
        var4=self.var4.get()

      
        
        kratica2=slovar[var2]
        kratica3=slovar[var3]
        kratica4=slovar[var4]
        stran5=requests.get("https://www.cia.gov/library/publications/the-world-factbook/rankorder/2119rank.html")
        stran2=requests.get("https://www.cia.gov/library/publications/the-world-factbook/geos/{0}.html".format(str(kratica2)))
        stran3=requests.get("https://www.cia.gov/library/publications/the-world-factbook/geos/{0}.html".format(str(kratica3)))
        stran4=requests.get("https://www.cia.gov/library/publications/the-world-factbook/geos/{0}.html".format(str(kratica4)))

        y_stretch=30
        x_stretch=100
        y_gap=0
        x_gap=20
        x_width=20
        

        try:

            if var=="area":
                ar2=re.findall(r'<span class=category>total: </span><span class=category_data>(.*?)</span></div>',str(stran2.content))
                are2=ar2[0].split(" ")
                area2=are2[0].replace(",","")
                ar3=re.findall(r'<span class=category>total: </span><span class=category_data>(.*?)</span></div>',str(stran3.content))
                are3=ar3[0].split(" ")
                area3=are3[0].replace(",","")
                ar4=re.findall(r'<span class=category>total: </span><span class=category_data>(.*?)</span></div>',str(stran4.content))
                are4=ar4[0].split(" ")
                area4=are4[0].replace(",","")
              

                podatki=[int(area2),int(area3),int(area4)]
                podatki2=[are2,are3,are4]
                for x,y in enumerate(podatki):
                    x1=x * x_stretch + x * x_width + x_gap
                    y1 = self.g_height - (y/1000000 * y_stretch + y_gap)
                    x2=x * x_stretch + x * x_width + x_width + x_gap
                    y2=self.g_height - y_gap

                    self.platno.create_rectangle(x1,y1,x2,y2,fill="green")
                    self.platno.create_text(x1+2,y1,anchor=SW,text=str(y))
                
            else:
                
                po2=re.findall('.html>{0}</a></td><td>(.*?)</td><td>July 2014 est.</td></tr>'.format(var2),str(stran5.content))
                pop2=po2[0].replace(",","")
                po3=re.findall('<a href=../geos/{0}.html>{1}</a></td><td>(.*?)</td><td>July 2014 est.</td></tr>'.format(kratica3,var3),str(stran5.content))
                pop3=po3[0].replace(",","")
                po4=re.findall('<a href=../geos/{0}.html>{1}</a></td><td>(.*?)</td><td>July 2014 est.</td></tr>'.format(kratica4,var4),str(stran5.content))
                pop4=po4[0].replace(",","")


                podatki=[int(pop2),int(pop3),int(pop4)]
                
                
               
                
                for x,y in enumerate(podatki):
                    x1=x * x_stretch + x * x_width + x_gap
                    y1 = self.g_height - (y/10000000 * y_stretch + y_gap)
                    x2=x * x_stretch + x * x_width + x_width + x_gap
                    y2=self.g_height - y_gap

                    self.platno.create_rectangle(x1,y1,x2,y2,fill="green")
                    self.platno.create_text(x1+2,y1,anchor=SW,text=str(y))
            
        except KeyError:
            print ("You entered the wrong name. ")
       
            
    
                
               

       
            







root = Tk()
root.title("Basic informations about countries")
root.option_add("*background", "white")
root.configure(background='white')
aplikacija=Countries(root)
root.mainloop()        
